#Simulirajte bacanje igraće kocke 30 puta. Izračunajte srednju vrijednost 
#rezultata i standardnu devijaciju. Pokus ponovite 1000 puta. 
#Prikažite razdiobu dobivenih srednjih vrijednosti. Što primjećujete?
#Kolika je srednja vrijednost i standardna devijacija dobivene razdiobe?
#Što se događa s ovom razdiobom ako pokus ponovite 10000 puta?

import numpy as np
import matplotlib.pyplot as plt


sttdev=np.zeros(1000);
avg=np.zeros(1000);
iter=0;
br=0
while(iter<1000):       #1000 iteracija
    k=np.random.choice([1,2,3,4,5,6],size=30)  #bacanje kocke
    print k
    x=sum(k)/30;        #srednja vrijednost
    stdev=np.std(k)      #standard deviation
    sttdev[br]=stdev
    print sttdev
    avg[br]=x
    br=br+1 
    iter=iter+1
plt.hist(avg)
plt.hist(sttdev)
plt.show    