#Git repozitorij https://gitlab.com/rgrbic/data_LV2.git sadrži datoteku 
#mtcars.csv koja sadrži različita mjerenja provedena na 32 automobila 
#(modeli 1973-74). Prikažite ovisnost potrošnje automobila (mpg) o konjskim 
#snagama (hp). Na istom grafu prikažite i informaciju o težini pojedinog vozila.

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv') #čitanje csv.dokumenta
X= mtcars['mpg'].tolist()
Y= mtcars['hp'].tolist()
C= mtcars['wt'].tolist()

from pylab import *
fig = gcf()
ax = fig.gca()

s = ax.scatter(X,Y,c=C) 

cb = plt.colorbar(s)    
cb.set_label('Masa  [klbs]')
plt.title("Automobili")
plt.xlabel('Potrosnja [mpg]')
plt.ylabel('Snaga [ks]')
xlim(10,30)
ylim(80,280)