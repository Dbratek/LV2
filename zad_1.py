##Napišite Python skriptu koja će učitati tekstualnu datoteku, pronaći valjane 
##email adrese te izdvojiti samo prvi dio adrese (dio ispred znaka @). 
##Koristite odgovarajući regularni izraz. Koristite datoteku mbox-short.txt.
# Ispišite rezultat
import re

fhand = open('mbox_short.txt')

for line in fhand:
    line = line.rstrip()
    lst = re.findall('(\S+)@\S+',line) 
    if len(lst)>0:
        print lst